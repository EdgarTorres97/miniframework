<?php
require_once ROOT_PATH.'/libs/controller.php';
class PagesController extends Controller{
    public function getNosotros(){
        return new view('pages/nosotros',['titulo'=>'Somos Patito SA de CV']);
    }
    public function getContacto(){
        return new view('pages/contacto',['titulo'=>'Contactanos']);
    }
}