<?php
$listadetareas=$this->tareas;
$vtareas=$this->vtareas;
/*
foreach($listadetareas as $latarea){
    echo 'El nombre de la tarea es: '.$latarea->Titulo.' El usuario asignado es: '. $latarea->nombre .'<br>';//campo de la tabla tareas
}*/
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/vencidas.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <title>Document</title>
</head>
<body>
    <header>
        <h1>Monitor Tareas</h1>
        <h5>Luis Hernández<a href="tareasvencidas.html"> (Salir) </a></h5>
    </header>
    <section id="cuerpo">
            <button class="agregar">Agregar Tarea</button>
        <h2>Tareas vencidas</h2>
        <table class="table" cellspacing="0">
                <thead>
                    <tr >
                        <th>#</th><!--esta es una columna-->
                        <th>Tarea</th><!--esta es una columna-->
                        <th>Asignado por</th><!--esta es una columna-->
                        <th>Fecha de vencimiento</th><!--esta es una columna-->
                    </tr>
                    </thead>
                    <tbody>
                        <?php 
                        foreach($listadetareas as $latarea){
                        ?>
                    <tr>
                        <td><?php echo $latarea->id_tarea;?></td><!--esta es una columna-->
                        <td><?php echo $latarea->Titulo;?></td><!--esta es una columna-->
                        <td><?php echo $latarea->nombre;?></td><!--esta es una columna-->
                        <td><?php echo $latarea->fecha_vencimiento;?></td><!--esta es una columna-->

                    </tr>
                        <?php } ?>
                </tbody>
            </table>
            <h2>Tareas por realizar</h2>

            <table class="table" cellspacing="0">
                    <thead>
                            <tr class="colo">
                                    <th>#</th><!--esta es una columna-->
                                    <th>Tarea</th><!--esta es una columna-->
                                    <th>Asignado por</th><!--esta es una columna-->
                                    <th>Fecha de vencimiento</th><!--esta es una columna-->
                                </tr>
                                
                        </thead>
                        <tbody>
                        <?php 
                        foreach($vtareas as $lavtarea){
                        ?>
                        <tr>
                            <td><?php echo $lavtarea->id_tarea;?></td><!--esta es una columna-->
                            <td><?php echo $lavtarea->Titulo;?></td><!--esta es una columna-->
                            <td><?php echo $lavtarea->nombre;?></td></td><!--esta es una columna-->
                            <td><?php echo $lavtarea->fecha_vencimiento;?></td><!--esta es una columna-->
    
                        </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>
                <h2>Tareas asignadas a otros</h2>

                <table class="table"  cellspacing="0">
                        <thead>
                                <tr class="colo">
                                        <th>#</th><!--esta es una columna-->
                                        <th>Tarea</th><!--esta es una columna-->
                                        <th>Asignado por</th><!--esta es una columna-->
                                        <th>Fecha de vencimiento</th><!--esta es una columna-->
                                    </tr>
                            </thead>
                            <tbody>
                        </tbody>
                    </table>
            <button class="agregar">Editar</button>
    </section>
    <script src="js/jquery-3.4.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>